# felix-container

### Motivation
`felix` builds depend on `LCG` which is mostly used from `cvmsfs`
Using `cvmfs` does not guarantee a persistent version management or
reprodicble builds. In addition, it is useful to be able to deploy 
felix applications from a self contained docker image

The repository provides scripts to build docker containers for `felixbase` and `felix-star`
* `felixcore` :  https://gitlab.cern.ch/atlas-tdaq-felix/felixcore
* `felix-star`:  https://gitlab.cern.ch/atlas-tdaq-felix/felix-star

The resulting containers are self contained and do not depend on `cvmfs`

### Building docker containers
To build containers run

```./scripts/build_docker.sh felixcore```

or

```./scripts/build_docker.sh felix-star```

### Running the containers

```docker run -ti felix/felixcore```

or

```docker run -ti felix/felix-star```





