#!/bin/bash

prod=${1}
if [ -z ${prod} ]; then
  echo "No build product specified"
  exit 1
fi
echo "Build LCG container"
docker build -f Dockerfile-lcg . --tag=felix/lcg ${2}
docker build -f Dockerfile-build . --build-arg PRODUCT=${prod} --tag=felix/build-${prod} ${2}
docker build -f Dockerfile-install . --build-arg PRODUCT=${prod} --tag=felix/${prod} ${2}
