#!/usr/bin/bash

declare -A pkg

lcg_prefix="LCG"
binary_tag="${BINARY_TAG}"
gcc_tag=$(echo ${binary_tag} | sed s/-gcc11-opt//g)
lcg_version="101"

lcg_arch=$(echo ${binary_tag} | sed s/-/_/g)
prod=${1}
source /bin/pkg.sh ${prod}

yum="dnf -y --disablerepo=* --enablerepo=lcgrepo7101,lcg-contrib7"
gcc_ver=${pkg["gcc"]}
gcc_major=$(echo ${pkg["gcc"]} | awk -F. '{print $1}')

gcc_arch=$(echo ${lcg_arch} | sed s/_gcc11_opt//g)

install=""

touch /etc/ld.so.conf.d/lcg.conf

for key in ${!pkg[@]}; do
    version=${pkg[${key}]}
    arch=${lcg_arch}
    libdir=lib
    tag=${binary_tag}
    if [  ${key} == "gcc" ]
    then
      arch=${gcc_arch}
      libdir=lib64
      tag=${gcc_tag}
    fi
    install="${install} ${lcg_prefix}_${lcg_version}_${key}_${version}_${arch}"
    echo "/opt/lcg/releases/LCG_${lcg_version}/${key}/${version}/${tag}/${libdir}" >> /etc/ld.so.conf.d/lcg.conf
done
echo "Installing: ${install}"
${yum} install ${install}

#  fix LCG setup links
mkdir -p /opt/lcg/contrib/gcc/
cd /opt/lcg/contrib/gcc/
ln -s ../../gcc/${gcc_ver} ${gcc_major}

cmake_ver=${pkg["CMake"]}
mkdir -p /opt/lcg/contrib/CMake/${cmake_ver}
cd /opt/lcg/contrib/CMake/${cmake_ver}
cmake_dir=$(ls -d ../../../CMake/${cmake_ver}-*)
ln -s ${cmake_dir}/${binary_tag} Linux-x86_64

cd /opt/lcg
ln -s  . releases
ldconfig
