#!/usr/bin/bash

product=${1}
git clone https://:@gitlab.cern.ch:8443/atlas-tdaq-felix/${product}.git
cd ${product}
git submodule init
git submodule update
source cmake_tdaq/bin/setup.sh ${BINARY_TAG}
cmake_config ${BINARY_TAG}
cd ${BINARY_TAG}
make -j4
make -j4 install
