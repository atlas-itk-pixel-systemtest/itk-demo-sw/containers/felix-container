#!/usr/bin/bash
product=${1}
version_file="cmake_tdaq/cmake/modules/FELIX-versions.cmake"
echo "Installing product ${product}"
cd /build/${product}
cp -r ./installed /

for pkg in $(ls external/) ; do
version=$(grep  ${pkg} ${version_file} | awk '{print $2}' | sed s/\)//g)
if [ -e "external/${pkg}/${version}/${BINARY_TAG}" ] 
then
  echo external/${pkg}/${version}/${BINARY_TAG}/
  rsync -avr external/${pkg}/${version}/${BINARY_TAG}/ /installed/${BINARY_TAG}/
fi
done
# copy RCC/CMEM libs
cp drivers_rcc/lib64/* /installed/lib/

cp ${BINARY_TAG}/lib*.so* /installed/lib/

for f in $(find /installed -type f | grep -v \.debug)
do
  file=$(file $f | grep "ELF 64-bit")
  if [ ! -z "${file}" ]
  then
     patchelf --set-rpath /installed/lib:/installed/${BINARY_TAG}/lib:/installed/${BINARY_TAG}/lib64 ${f}
  fi
done
rm -rf /build

